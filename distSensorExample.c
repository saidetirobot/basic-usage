/*
 * distSensorExample
 *
 * Application example of distance sensors in the DETI robot
 *
 * pf@ua.pt
 */

#include "mr32.h"

#define DISTANCE  20
#define DELTA      2
#define VEL       60

int
main (void)
{
  /* Variable declarations go here */
  unsigned int SensFront;

  /* initPIC32() makes all the required initializations to the
  * PIC32 hardware platform */
  initPIC32 ();

  /* Open Loop Control */
  closedLoopControl(0);

  enableObstSens();

  while (1)
  {
    /* Code goes here */
    readAnalogSensors();
    SensFront = obstacleSensor(OBST_SENSOR_FRONT);
    printf("%d\n",SensFront);


    if(SensFront > DISTANCE+DELTA) {
      setVel2(VEL,VEL);
      leds(0x01);
    }
    else if(SensFront < DISTANCE - DELTA){
      setVel2(-VEL,-VEL);
      leds(0x02);
    }
    else{
      setVel2(0,0);
      leds(0x03);
    }

  }
  return (0);
}
