/*
 +  lineSensorOnOff
 *
 *  Switches ground sensors in DETI robot on and off
 *
 *  pf@ua.pt
 */

#include "mr32.h"

int
main (void)
{
  /* Variable declarations go here */

  /* initPIC32() makes all the required initializations to the
   * PIC32 hardware platform */
  initPIC32 ();

  while (1)
    {
      enableGroundSens();
      wait(10);
      printf(".");
      disableGroundSens();
      wait(10);
      printf(".");

    }
  return (0);
}
