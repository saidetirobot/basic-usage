/*
 *  lineSensorExample
 *
 * Example of reading line sensors in DETI robot.
 *
 * The code also includes a simple example on how to display the bit map
 * value in decimal, hexadecimal and binary representation, as well as
 * how to display the read values without scrolling the terminal screen.
 *
 * pf@ua.pt
 */

#include "mr32.h"

int
main (void)
{
  /* Variable declarations go here */
  unsigned int line;
  /* initPIC32() makes all the required initializations to the
   * PIC32 hardware platform */
  initPIC32 ();

  enableGroundSens();

  printf("Dec\tHex\tBinary\n");
  while (1)
    {
      int i;

      /* Read line with default values */
      line = readLineSensors(0);

      /* Print value in decimal */
      printf("%02d\t", line);

      /* Print value in hexadecimal */
      printf("%02x\t", line);

      /* Print value in binary */
      for(i=0;i<5;i++){
        printf("%d",line&0x01);
        line = line >> 1;
      }
      printf("\r");

      wait(1);
    }
  return (0);
}
